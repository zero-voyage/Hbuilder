window.addEventListener('load',function() {
	var focus=document.querySelector('.focus');
	var ul=focus.children[0];
	var w=focus.offsetWidth;
	var ol=focus.children[1];
	// 获得focus的宽度
	var index=0;
	var timer=setInterval(function() {
		index++;
		var translateX=-index*w;
		ul.style.transition='all .3s';
		ul.style.transform='translateX('+translateX+'px)';
	},2000)
	// 等着过度结束后，再去判断 监听过度完成的事件 transitionend
	ul.addEventListener('transitionend',function() {
		if(index>=3) {
			index=0;
			// 去掉过度效果
			ul.style.transition='none';
			var translateX=-index*w;
			ul.style.transform='translateX('+translateX+'px)';
		}else if(index<0) {
			index=0;
			ul.style.transition='none';
			var translateX=-index*w;
			ul.style.transform='translateX('+translateX+'px)'; 
		}
		// 小圆点跟随变化效果
		ol.querySelector('.current').classList.remove('current');
		ol.children[index].classList.add('current');
	});
	
	// 手指滑动轮播图
	var startX=0;
	var moveX=0;
	var flag=false;
	ul.addEventListener('touchstart',function(e) {
		startX=e.targetTouches[0].pageX;
		// 手指触摸时停止定时器
		clearInterval(timer);
	});
	ul.addEventListener('touchmove',function(e) {
		// 计算距离
		moveX=e.targetTouches[0].pageX-startX;
		// 移动盒子
		var translateX=-index*w+moveX;
		// 手指拖动不需要动画效果
		ul.style.transition='none';
		ul.style.transform='translateX('+translateX+'px)'; 
		flag=true;//如果用户手指移动过我们再去判断否则不做判断效果
		e.preventDefault();
	});
	ul.addEventListener('touchend',function(e) {
		if(flag) {
			// 如果移动距离大于50像素我们就播放上一张或者下一张
			if(Math.abs(moveX)>50) {
				// 右滑就是播放上一张 是正值
				if(moveX>0) {
					index--;
				} else {
					index++;
				}
				var translateX=-index*w;
				ul.style.transition='all .3s';
				ul.style.transform='translateX('+translateX+'px)'; 
				// 左滑就是播放下一张 是负值
			}else {
				// 回弹效果
				var translateX=-index*w;
				ul.style.transition='all .1s';
				ul.style.transform='translateX('+translateX+'px)';
			}
		}
		clearInterval(timer);
		timer=setInterval(function() {
			index++;
			var translateX=-index*w;
			ul.style.transition='all .3s';
			ul.style.transform='translateX('+translateX+'px)';
		},2000)
	})
	// 返回顶部模块制作
	var goback=document.querySelector('.goBack');
	var nav=document.querySelector('nav');
	window.addEventListener('scroll',function() {
		if(window.pageYOffset>=nav.offsetTop) {
			goback.style.display='block';
		}else {
			goback.style.display='none';
		}
	});
	goback.addEventListener('click',function() {
		window.scroll(0,0);
	})
})