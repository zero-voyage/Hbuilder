$(function() {
	var toolTop=$('.recommend').offset().top;
	toggleTool();
	var flag=true;
	function toggleTool() {
		if($(document).scrollTop()>=toolTop) {
			$('.fixedtool').fadeIn();
		}else {
			$('.fixedtool').fadeOut();
		};
	}
	$(window).scroll(function() {
		if(flag) {
			toggleTool();
			$('.floor .w').each(function(i,ele) {
				if($(document).scrollTop()>=$(ele).offset() .top) {
					$('.fixedtool li').eq(i).addClass('current').siblings().removeClass();
				}
			})
		}
	});
	// 点击电梯滚动到相对区域
	$('.fixedtool li').click(function() {
		flag=false;
		// 选出对应索引号的内容去区的盒子 计算它的top值
		var current=$('.floor .w').eq($(this).index()).offset().top;
		$('body,html').stop().animate({
			scrollTop:current
		},function() {
		flag=true;
		});
		$(this).addClass('current').siblings().removeClass()
	})
})