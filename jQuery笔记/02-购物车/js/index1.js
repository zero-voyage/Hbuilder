$(function() {
	$('.checkall').change(function() {
	// $(this).prop('checked');
		$('.j-checkbox,.checkall').prop('checked',$(this).prop('checked'));
	})
	// 通过小的复选框来进行全选按钮
	$('.j-checkbox').change(function() {
		if ($('.j-checkbox:checked').length===$('.j-checkbox').length) {
			$('.checkall').prop('checked',true);
		}else {
			$('.checkall').prop('checked',false);
		}
	});
	// 3.增减商品数量模块 首先声明一个变量，当我们点击+号(increment)，就让这个值++，然后赋值给文本框。
	$('.increment').click(function() {
		var n=$(this).siblings('.itxt').val();
		n++;
		$(this).siblings('itxt').val(n);
	// 4.计算小计模块 根据文本框的值 乘以 当前商品的价格 就是 商品的小计
		var p=$(this).parent().parent().siblings('.p-price').html();
		p=p.substr(1);
		$(this).parent().parent().siblings('.p-sum').html('￥'+p*n);
	});
	$('.decrement').click(function() {
		var n=$(this).siblings('.itxt').val();
		if(n==1) {
			return false;
		}
		n--;
		$(this).siblings('itxt').val(n);
		var p=$(this).parent().parent().siblings('.p-price').html();
			p=p.substr(1);
			$(this).parent().parent().siblings('.p-sum').html('￥'+p*n);
		});
	});