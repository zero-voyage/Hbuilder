function animate(obj, target, callback) {
	clearInterval(obj.timer);
	// 回调函数写到定时器里面
	if (callback) {
		// 调用函数
		callback();
	}
	obj.timer = setInterval(function() {
		// var step=Math.ceil((target-obj.offsetLeft)/10);
		// 把我们步长值改成整数，不要出现小数
		var step = (target - obj.offsetLeft) / 10;
		step = step > 0 ? Math.ceil(step) : Math.floor(step);
		if (obj.offsetLeft == target) {
			clearInterval(obj.timer);
		} else {
			// 把每次+1这个值改成一个慢慢变小的值（目标值-现在的位置）/10
			obj.style.left = obj.offsetLeft + step + 'px';
		}
	}, 30);
}
