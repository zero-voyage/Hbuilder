// 主要职责：创建一个VUE应用（理解为之前的根实例）

// 1.导入createApp函数从vue中

// 2.创建一个根组件App.vue导入至main

// 3.使用createApp创建应用

// 4.挂载到我们的#app容器里
import { createApp } from 'vue'
import './index.css'
import App from './App.vue'
const app = createApp(App)
// app.mixin({
// 	mounted() {
// 		console.log(this.$el, 'dom准备好的')
// 	} 
// })
app.mount('#app')
// createApp(App).mount('#app')
